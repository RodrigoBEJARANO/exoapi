
var btn = document.getElementById("btn");


btn.addEventListener("click", function () {
    
    // Obtenir le nom du departement
    var nroDepto = document.getElementById("nroDepto").value;
    var monRequete = new XMLHttpRequest();
    monRequete.open('GET', 'https://geo.api.gouv.fr/departements?code=' +nroDepto +'&fields=code');
    monRequete.onload = function () {
        var monData = JSON.parse(monRequete.responseText);
        showDpto(monData);
        //console.log(monData[0]);
    }
    monRequete.send();
    
    // Obtenir liste de communes 
    
    var maRequetteListe = new XMLHttpRequest();
    maRequetteListe.open('GET', 'https://geo.api.gouv.fr/communes?codeDepartement=' + nroDepto + '&fields=nom,code,codesPostaux,codeDepartement,codeRegion,population&format=json&geometry=centre');
    
    maRequetteListe.onload = function(){
        var monDataListe = JSON.parse(maRequetteListe.responseText);
        populate(monDataListe);
        
    }
    maRequetteListe.send();
    

});


function showDpto(monData){
    document.getElementById("nomDepto").innerHTML= monData[0].nom;

}

function populate(maData) {
    var listeComm = document.getElementById("listeComm");
    for (i = 0; i < maData.length; i++) {
        var opValue = document.createElement("OPTION");
        var txt = document.createTextNode(maData[i].nom);
        opValue.appendChild(txt);
        listeComm.insertBefore(opValue, listeComm.lastChild);

    }

}

function fiche() {
    var nroDepto = document.getElementById("nroDepto").value;
    var commune = document.getElementById("listeComm").value;
    var maRequetteListe = new XMLHttpRequest();
    
    maRequetteListe.open('GET', 'https://geo.api.gouv.fr/communes?nom='+ commune +'&codeDepartement=' + nroDepto + '&fields=nom,code,codesPostaux,codeDepartement,codeRegion,population&format=json&geometry=centre');

    maRequetteListe.onload = function () {
        var monDataListe = JSON.parse(maRequetteListe.responseText);
        showInfo(monDataListe);

    }
    maRequetteListe.send();

    console.log(commune);
}


function showInfo(maData){
    var villeInfo = document.getElementById("villeInfo");
   
    var infoString = "Ville: " + maData[0].nom + " *  CP: " + maData[0].codesPostaux + " *  Population: " + maData[0].population ;
    
    villeInfo.insertAdjacentHTML('beforeend',infoString);

    
}